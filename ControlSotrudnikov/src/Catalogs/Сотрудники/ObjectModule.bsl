
Процедура ПриЗаписи(Отказ)
	//\это новый комментарий
	Если НЕ Отказ И ЭтоНовыйСотрудник() Тогда
		Менеджер = РегистрыСведений.СостоянияСотрудников.СоздатьМенеджерЗаписи();
		Менеджер.Период = ТекущаяДатаСеанса();
		Менеджер.Состояние = Перечисления.СостоянияСотрудников.Свободен;
		Менеджер.Сотрудник = ЭтотОбъект.Ссылка;
		Менеджер.Комментарий = "Новый сотрудник. Записан: " + Менеджер.Период;
		Менеджер.Записать();
	КонецЕсли;
	
КонецПроцедуры

Функция ЭтоНовыйСотрудник()
	
	Набор = РегистрыСведений.СостоянияСотрудников.СоздатьНаборЗаписей();
	Набор.Отбор.Сотрудник.Установить(ЭтотОбъект.Ссылка);
	Набор.Прочитать();
	Если Набор.Количество() = 0 Тогда
		Возврат Истина;
	Иначе
		Возврат Ложь;
	КонецЕсли;
КонецФункции


Процедура ПередЗаписью(Отказ)
	
	Если НЕ Отказ И ЭтоНовыйСотрудник() Тогда
		ИдентификаторПользователяИБ = "";
	КонецЕсли;
	
КонецПроцедуры

